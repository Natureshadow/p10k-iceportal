# Powerlevel10k prompt segments for ICEportal.de

function _is_wifionice {
    # Return 0 if connected to a known ICE wifi network
    LC_ALL=C \
	  nmcli -t -f active,ssid dev wifi | \
	grep "^yes:" | \
	grep \
	    -e WIFIonICE \
	    -e WIFI@DB \
	    >/dev/null 2>&1
}

function _get_cache_file {
    # Determine and create cache directory

    local api=$1; shift

    XDG_CACHE_HOME=${XDG_CACHE_HOME:-~/.cache}
    local cache_dir=${XDG_CACHE_HOME}/p10k-iceportal
    local cache_file=${cache_dir}/$(basename ${api}).json
    mkdir -p "${cache_dir}"

    echo "${cache_file}"
}

function _is_stale {
    local api=$1; shift
    local cache_file=$(_get_cache_file "${api}")

    # Calculate mtime diff
    local max_diff=30
    local mtime=0; [[ -e "${cache_file}" ]] && mtime=$(date -r "${cache_file}" +"%s")
    local now=$(date +"%s")
    local diff=$(( now - mtime ))
    [[ ${diff} -gt max_diff ]]
}

function _refresh_data {
    # Refresh a data file if necessary

    _is_wifionice || return

    local api=$1; shift
    local cache_file=$(_get_cache_file "${api}")

    _is_stale "${api}" || return

    # API URL to iceportal.de
    local url="https://iceportal.de/api1/rs/${api}"

    curl -s "${url}" --output "${cache_file}"
}

function _get_value {
    # Get a value from the API endpoints by API name and jq query

    local api=$1; shift
    local jq_path=$1; shift

    cache_file=$(_get_cache_file "${api}")

    jq -r ".${jq_path}" "${cache_file}"
}

function prompt_iceportal {
    for api in tripInfo/trip; do
	_is_stale "${api}" && (_refresh_data "${api}" &)
    done

    if ! _is_stale tripInfo/trip; then
	train_type=$(_get_value tripInfo/trip trip.trainType)
	vzn=$(_get_value tripInfo/trip trip.vzn)

	p10k segment -b 195 -i "🚄" -t "${train_type}${vzn}"
    fi
    if ! _is_stale status; then
	connectivity_current=$(_get_value status connectivity.currentState)

	# FIXME p10k segment
    fi
}

function instant_prompt_iceportal {
    prompt_iceportal
}
