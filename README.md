# p10k-iceportal

This is a user-defined segment for the [powerlevel10k ZSH-theme](https://github.com/romkatv/powerlevel10k) that queries data about the high speed train (ICE) of the Deutsche Bahn if the PC is connected to the trains wi-fi.

## Installation

The installation method has quite some room for improvements:

1. Make sure that the dependencies `nmcli`, `grep` and `jq` are available
2. Open the configuration of p10k located under `~/.p10k.zsh`
3. Go to the bottom of the file and paste the code there
4. Depending if you want to have the segment located at the left or at the right, go to the section that defines `POWERLEVEL9K_LEFT_PROMPT_ELEMENTS` respectively `POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS` and add `iceportal` to the list.
5. Restart your shell for good measure